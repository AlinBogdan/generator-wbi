import React  , {useState}                    from 'react'
import {View,Text,TouchableOpacity,TextInput} from 'react-native'
import styles                                 from './<%= name %>.style'
import {useRedux<%= Name %>} from  "../_redux/useRedux<%= Name %>"

export const <%= Name %> = ( ) => {
  const {
  <%= name %>,result
} = useRedux<%= Name %>();

  <%- listInput.map(value => `const [${value.toLowerCase()},set${value.charAt(0).toUpperCase() + value.slice(1)}]=useState(\'\') ; 
  ` ).join('')  %>

  const onPress = () => {
    <%= name %>({
    <%- listInput.map(value => `${value.toLowerCase()}:${value.toLowerCase()}` )  %>
    })
    }

    return (
    <View style={styles.container}>
      <Text>Page : <%= name %></Text>
      <>
      <Text>Input :  </Text>
      <%- listInput.map(value => `<TextInput
        onChangeText={val => {
        set${value.charAt(0).toUpperCase() + value.slice(1)}(val);
      }}
      placeholder={'${value.toLowerCase()}'}
    />` ).join('')  %>
        <>
      <TouchableOpacity
        onPress={onPress}
        style={{
          borderWidth: 2,
          borderColor: 'black',
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <Text>CALL  <%= name %> </Text>
      </TouchableOpacity>
      <Text>Output:{JSON.stringify(result)}</Text>
    </View>
  )
}


